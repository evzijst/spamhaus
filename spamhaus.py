#!/usr/bin/env python

import socket
import sys

for line in [line.strip() for line in sys.stdin.readlines()]:
    try:
        host = socket.gethostbyname(line)
        revip = '.'.join(host.split('.')[::-1])
        try:
            socket.gethostbyname(revip + '.zen.spamhaus.org')
            print '%s is part of a botnet' % line
        except IOError: pass
    except IOError:
        print 'failed to resolve %s' % line
